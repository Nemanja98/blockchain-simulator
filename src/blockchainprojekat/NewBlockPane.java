/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockchainprojekat;

import blockchainprojekat.model.Block;
import blockchainprojekat.model.Blockchain;
import blockchainprojekat.util.BlockchainIO;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Nemanja Pantelic
 */
public class NewBlockPane {

    
    private ScrollPane scrollPane = new ScrollPane(); 
    private TitledPane titledPane = new TitledPane();
    private VBox pane = new VBox();

    private FlowPane blockCanvas;

    private GridPane entryPane = new GridPane();

    private ArrayList<TextField> senders = new ArrayList<>();
    private ArrayList<TextField> receivers = new ArrayList<>();
    private ArrayList<TextField> amounts = new ArrayList<>();

    private Button newBlockBtn = new Button();
    private Button newEntryBtn = new Button();
    Button valdiateBtn = new Button();
    
    private int entries = 0;
    private int k = 0;
    
    ArrayList<String> data = new ArrayList<>();
    
    private Blockchain chain = BlockchainIO.loadBlockchain();

    public NewBlockPane(FlowPane blockCanvas) {

        this.blockCanvas = blockCanvas;

        newEntryBtn.setText("Add new entry");
        newBlockBtn.setText("Add new block");
        valdiateBtn.setText("Validate Blockchain");

        entryPane.setHgap(10);
        entryPane.setVgap(10);
        
        setupEntries();
        
        valdiateBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               

                boolean chainValid = chain.isChainValid();
                System.out.println("Valdiate Chain: " + chainValid );
                Alert.AlertType type = chainValid ? Alert.AlertType.INFORMATION : Alert.AlertType.ERROR;
                String message = chainValid ? "Blockchain is valid" : "Blockchain is invalid";
                        
                Alert alert = new Alert(type, message, ButtonType.OK);
                alert.show();

            }
        });
        
        newEntryBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("New Entry");

                senders.add(new TextField());
                receivers.add(new TextField());
                amounts.add(new TextField());
                entryPane.add(senders.get(entries), 0, entries + 1);
                entryPane.add(receivers.get(entries), 1, entries + 1);
                entryPane.add(amounts.get(entries), 2, entries + 1);
                entries++;

            }
        });
        
        newBlockBtn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("New block");

                
                for(int i=0;i<senders.size();i++){
                   String entry = senders.get(i).getText() + "-" + receivers.get(i).getText() + "-" + amounts.get(i).getText();
                   data.add(entry);
                }
                
                String previousHash = "0";
                
                if(chain.getBlockNumber() != 0)
                    previousHash = chain.getBlock(chain.getBlockNumber()-1).getHash();
                
                String[] dataArray = new String[data.size()];
                
                for(int j=0;j<data.size();j++)
                    dataArray[j] = data.get(j);
                
                Block newBlock = new Block(dataArray,previousHash,chain.getBlockNumber());
                chain.addBlock(newBlock);
                
                entryPane.getChildren().clear();
                
                setupEntries();
                
                Group group = new BlockShape(newBlock).getShapeGroup();
                k++;
                blockCanvas.getChildren().add(group);
                data = new ArrayList<>();

            }
        });

        GridPane buttonPane = new GridPane();

        buttonPane.setHgap(10);
        buttonPane.setVgap(10);
        
        buttonPane.add(newBlockBtn,0,0);
        buttonPane.add(newEntryBtn,2,0);
        buttonPane.add(valdiateBtn,3,0);

        pane.getChildren().add(buttonPane);
        pane.getChildren().add(entryPane);
        
        titledPane.setContent(pane);
        
        titledPane.setText("Create New Block");
        
        startup();
        
        scrollPane.setContent(titledPane);
        
    }

    public TitledPane getPane() {
        return titledPane;
    }

    private void setupEntries() {

        entries = 0;
        
        senders = new ArrayList<>();
        receivers = new ArrayList<>();
        amounts = new ArrayList<>();

        Label sender = new Label("Sender");
        Label receiver = new Label("Receiver");
        Label amount = new Label("Amount");

        entryPane.add(sender, 0, 0);
        entryPane.add(receiver, 1, 0);
        entryPane.add(amount, 2, 0);

        senders.add(new TextField());
        receivers.add(new TextField());
        amounts.add(new TextField());
        entryPane.add(senders.get(entries), 0, entries + 1);
        entryPane.add(receivers.get(entries), 1, entries + 1);
        entryPane.add(amounts.get(entries), 2, entries + 1);
        entries++;
    }

    public Blockchain getChain() {
        return chain;
    }

    public void setChain(Blockchain chain) {
        this.chain = chain;
    }

    private void startup(){
        
        for(Block block : chain.getBlockchain()){
            Group group = new BlockShape(block).getShapeGroup();
            k++;
            blockCanvas.getChildren().add(group);
        }
        
    }
    
}
