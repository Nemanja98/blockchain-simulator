/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockchainprojekat.util;

import blockchainprojekat.model.Block;
import blockchainprojekat.model.Blockchain;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nemanja Pantelic
 */
public class BlockchainIO {

    public static final String DELIMITER = "======";

    public static void saveBlockchain(Blockchain chain) {

        try {
            FileWriter fileWriter = new FileWriter("blockchain.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            for (Block block : chain.getBlockchain()) {
                printWriter.println("BlockNumber");
                printWriter.println(block.getBlockNumber());
                printWriter.println("BlockHash");
                printWriter.println(block.getHash());
                printWriter.println("Nonce");
                printWriter.println(block.getNonce());
                printWriter.println("Timestamp");
                printWriter.println(block.getTimeStamp());
                printWriter.println("PreviousHash");
                printWriter.println(block.getPreviousHash());
                printWriter.println("Data");
                for (String s : block.getData()) {
                    printWriter.println(s);
                }

                printWriter.println(DELIMITER);
            }
            printWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(BlockchainIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Blockchain loadBlockchain() {

        Blockchain chain = new Blockchain();
        
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("blockchain.txt"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BlockchainIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            int blockNumber = 0;
            String blockHash= "";
            int nonce = 0;
            long Timestamp = 0;
            String PreviousHash = "";
            ArrayList<String> data = new ArrayList<>();
            boolean dataFound = false;

            while (line != null) {

                if (dataFound) {
                    if (!line.equals(DELIMITER)) {
                        data.add(line);
                    } else {
                        
                        String[] dataArray = new String[data.size()];
                
                        for(int j=0;j<data.size();j++)
                            dataArray[j] = data.get(j);
                        
                        Block block = new Block(blockHash, PreviousHash, dataArray, Timestamp, nonce, blockNumber);
                        chain.addBlockNoMine(block);
                        data = new ArrayList<>();
                        dataFound = false;
                    }

                } else if (line.equals("BlockNumber")) {
                    line = br.readLine();
                    blockNumber = Integer.parseInt(line);
                } else if (line.equals("BlockHash")) {
                    line = br.readLine();
                    blockHash = line;
                } else if (line.equals("Nonce")) {
                    line = br.readLine();
                    nonce = Integer.parseInt(line);
                } else if (line.equals("Timestamp")) {
                    line = br.readLine();
                    Timestamp = Long.parseLong(line);
                } else if (line.equals("PreviousHash")) {
                    line = br.readLine();
                    PreviousHash = line;
                } else if (line.equals("Data")) {
                    dataFound = true;
                }
                
                line = br.readLine();

            }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(BlockchainIO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return chain;
    }

}
