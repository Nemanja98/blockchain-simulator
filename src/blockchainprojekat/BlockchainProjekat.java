/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockchainprojekat;


import blockchainprojekat.model.Blockchain;
import blockchainprojekat.util.BlockchainIO;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Nemanja Pantelic
 */
public class BlockchainProjekat extends Application {
    
    int i =0;
    
    @Override
    public void start(Stage primaryStage) {
       
        HBox root = new HBox();
        ScrollPane scrolRoot = new ScrollPane(); 
        
        
        ScrollPane scrollPane = new ScrollPane(); 
        TitledPane titledPane = new TitledPane();
        FlowPane canvas = new FlowPane();
        

        NewBlockPane cmdPane = new NewBlockPane(canvas);
       
        titledPane.setContent(canvas);
        
        titledPane.setText("Blockchain");
       
        scrollPane.setContent(titledPane);
        //scrolRoot.setContent(root);
       
        HBox picturePane = new HBox();
        
        
        
        FileInputStream inputstream = null; 
        Image image = null;
        try {
            inputstream = new FileInputStream("How-To.png");
            image = new Image(inputstream);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BlockchainProjekat.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        ImageView imageView = new ImageView(image);
        picturePane.getChildren().add(imageView);
        
        TitledPane titledPicturePane = new TitledPane();
        titledPicturePane.setContent(picturePane);
        titledPicturePane.setText("How it works");
        
        
        root.getChildren().add(cmdPane.getPane());
        root.getChildren().add(scrollPane);
        root.getChildren().add(titledPicturePane);
        
        
        
        Scene scene = new Scene(root);
        
        primaryStage.setMaximized(true);
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        public void handle(WindowEvent we) {
            BlockchainIO.saveBlockchain(cmdPane.getChain());
        }
        }); 
        
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
