/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockchainprojekat.model;

import java.util.Date;

/**
 *
 * @author Nemanja Pantelic
 */
public class Block {
	
	public String hash;
	public String previousHash; 
	private String data[]; 
	private long timeStamp; 
	private int nonce;
        private int blockNumber;


	public Block(String[] data,String previousHash,int blockNumber) {
		this.data = data;
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();
		this.blockNumber = blockNumber;
		this.hash = calculateHash(); 
	}

        public Block(String hash, String previousHash, String[] data, long timeStamp, int nonce, int blockNumber) {
            this.hash = hash;
            this.previousHash = previousHash;
            this.data = data;
            this.timeStamp = timeStamp;
            this.nonce = nonce;
            this.blockNumber = blockNumber;
        }
        
        
	
	public String calculateHash() {
                
                String content = "";
                
                for(String s: data)
                    content += s;
                
		String calculatedhash = StringUtil.applySha256( 
				previousHash +
				Long.toString(timeStamp) +
				Integer.toString(nonce) + 
				content 
				);
		return calculatedhash;
	}
	

	public void mineBlock(int difficulty) {
		String target = StringUtil.getDificultyString(difficulty); 
		while(!hash.substring( 0, difficulty).equals(target)) {
			nonce ++;
			hash = calculateHash();
		}
		System.out.println("Block Mined!!! : " + hash);
	}

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    public int getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(int blockNumber) {
        this.blockNumber = blockNumber;
    }
   
}