/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blockchainprojekat;

import blockchainprojekat.model.Block;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 *
 * @author Nemanja Pantelic
 */
public class BlockShape {
    
    private Group shapeGroup = new Group();
    
    public BlockShape(Block block){
        
        String blockNumber = ""+block.getBlockNumber();
        String blockHash = block.getHash();
        String nonce = ""+block.getNonce();
        String previousBlockHash = block.getPreviousHash();
        String data[] = block.getData();
        
        double height = 100 + data.length * 15 + 60; 
        
        Rectangle rect1 = new Rectangle(0, 0, 620, height);
            rect1.setFill(Color.ANTIQUEWHITE);
        
            shapeGroup.getChildren().add(rect1);    
        
        shapeGroup.getChildren().add(new Text (310, 15, blockNumber));
        shapeGroup.getChildren().add(new Text (0, 30, "Previous Hash:"));
        shapeGroup.getChildren().add(new Text (100, 30, previousBlockHash));
        shapeGroup.getChildren().add(new Text (0, 45, "Nonce:"));
        shapeGroup.getChildren().add(new Text (50, 45, nonce));
        shapeGroup.getChildren().add(new Text (0, 60, "Block Hash:"));
        shapeGroup.getChildren().add(new Text (75, 60, blockHash));
        shapeGroup.getChildren().add(new Text (0, 75, "Data:"));
        
        int i=1;
        
        for(String s: data ){
            shapeGroup.getChildren().add(new Text (0, (75 + i*15), s));
            i++;
        }
        

    }

    public Group getShapeGroup() {
        return shapeGroup;
    }
    
    
    
}
